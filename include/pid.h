//
//  pid.h
//
//
//  Created by Scott Wigler on 11/30/15.
//
//

#ifndef _pid_h
#define _pid_h


typedef struct  pid{
    float kp, ki, kd;
    float out_min, out_max;
    float i_min, i_max, i_prev;
	float offset;
    float dt, error_prev;
    float error;

    float p,i,d;

    float output;

} pid_val;

void p_compute(pid_val *pid_data) {
    pid_data->p = pid_data->kp*pid_data->error;
}

void i_compute(pid_val *pid_data) {
    float i_temp = pid_data->ki * pid_data->error * pid_data->dt + pid_data->i_prev;
    i_temp = (i_temp < pid_data->i_min) ? pid_data->i_min : i_temp;
    i_temp = (i_temp > pid_data->i_max) ? pid_data->i_max: i_temp;

    pid_data->i = i_temp;
    pid_data->i_prev = i_temp;
}

void d_compute(pid_val *pid_data) {
    float d_temp = pid_data->kd * (pid_data->error - pid_data->error_prev)/(pid_data->dt);

    pid_data->d = d_temp;
}

void pid(pid_val *pid_data) {
    p_compute(pid_data);
	i_compute(pid_data);
	d_compute(pid_data);
	float out_temp = pid_data->p + pid_data->i + pid_data->d + pid_data->offset;

    out_temp=(out_temp < pid_data->out_min) ? pid_data->out_min:out_temp;
    out_temp=(out_temp > pid_data->out_max) ? pid_data->out_max:out_temp;

    pid_data->output = out_temp;
}

void p(pid_val *pid_data) {
    p_compute(pid_data);
	i_compute(pid_data);
	d_compute(pid_data);
	float out_temp = pid_data->p  + pid_data->offset;

    out_temp=(out_temp < pid_data->out_min) ? pid_data->out_min:out_temp;
    out_temp=(out_temp > pid_data->out_max) ? pid_data->out_max:out_temp;

    pid_data->output = out_temp;
}

void pi(pid_val *pid_data) {
    p_compute(pid_data);
	i_compute(pid_data);
	d_compute(pid_data);
	float out_temp = pid_data->p + pid_data->i + pid_data->offset;

    out_temp=(out_temp < pid_data->out_min) ? pid_data->out_min:out_temp;
    out_temp=(out_temp > pid_data->out_max) ? pid_data->out_max:out_temp;

    pid_data->output = out_temp;
}

void pd(pid_val *pid_data) {
    p_compute(pid_data);
	i_compute(pid_data);
	d_compute(pid_data);
	float out_temp = pid_data->p + pid_data->d + pid_data->offset;

    out_temp=(out_temp < pid_data->out_min) ? pid_data->out_min:out_temp;
    out_temp=(out_temp > pid_data->out_max) ? pid_data->out_max:out_temp;

    pid_data->output = out_temp;
}


#endif
