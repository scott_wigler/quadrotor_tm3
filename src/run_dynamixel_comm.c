#define EXTERN extern
#define DYNCOMM_FREQ 100 // in Hz
#include "../include/quadcopter_main.h"

void * run_dynamixel_comm(void * var){
    int hz = DYNCOMM_FREQ;
    double maxWheelTorque = .8;
    double minWheelTorque = 0.01;


    /* Initialize ports and addresses */
    Dynam_Init(&dynam);

    /* Configure Servo Number 1 to Wheel */
    pthread_mutex_lock(&dynamixel_mutex);
    bus.servo[0].id = 1;
    bus.servo[0].cmd_mode = WHEEL;
    bus.servo[0].cmd_torque = 1;
    bus.servo[0].cmd_speed = 0.1;
    bus.servo[0].cmd_flag = MODE;
    while(NONE != bus.servo[0].cmd_flag)
    {
        Dynam_SetMode(&dynam,&(bus.servo[0]));
    }
    bus.servo[0].cmd_speed = 0;
    bus.servo[0].cmd_flag = CMD;
    pthread_mutex_unlock(&dynamixel_mutex);

    /* Configure Servo Number 2 to Wheel */
    pthread_mutex_lock(&dynamixel_mutex);
    bus.servo[1].id = 2;
    bus.servo[1].cmd_mode = WHEEL;
    bus.servo[1].cmd_torque = 1;
    bus.servo[1].cmd_speed = 1;
    bus.servo[1].cmd_flag = MODE;
    while(NONE != bus.servo[1].cmd_flag)
    {
        Dynam_SetMode(&dynam,&(bus.servo[1]));
    }
    bus.servo[1].cmd_speed = 0;
    bus.servo[1].cmd_flag = CMD;
    pthread_mutex_unlock(&dynamixel_mutex);

    // These will need to be set in processing_loop.c, just here to test motors
    int activate = 0;
    int gripped[] = {0,0};
	struct perch perchstate;
	int i = 0;

    /* Infinity Loop */
    while(1){
        pthread_mutex_lock(&dynamixel_mutex);
		//printf("load %f %f \n",bus.servo[0].cur_load,bus.servo[1].cur_load);
		//printf("gripped %i %i %i\n", gripped[0], gripped[1], perchstate.release);
		i=!i;
		pthread_mutex_lock(&perch_mutex);
		memcpy(&perchstate, perch, sizeof(struct perch));
		pthread_mutex_unlock(&perch_mutex);

		switch(bus.servo[0].cmd_flag){
			case (NONE):
				break;
			case (CMD):
				Dynam_Command(&dynam,&(bus.servo[i]));
				bus.servo[i].cmd_flag = STATUS;
				break;
			case (MODE):
				Dynam_SetMode(&dynam,&(bus.servo[i]));
				bus.servo[i].cmd_flag = STATUS;
				break;
			case (STATUS):
				Dynam_Status(&dynam, &(bus.servo[i]));
				bus.servo[i].cmd_flag = STATUS;
				if (perchstate.release == 1) { //if commanded to perchstate.release
					//printf("problem %i %lf %lf\n",i, abs(bus.servo[i].cur_load), minWheelTorque);
					if(bus.servo[i].cur_load < -.9)//(bus.servo[i].cur_load < 0 && bus.servo[i].cur_load > -.1) ||
					{
						gripped[i] = 0;
						bus.servo[i].cmd_speed = 0;
						bus.servo[i].cmd_flag = CMD;
					}
					else
					{
						bus.servo[i].cmd_speed = -1;
						bus.servo[i].cmd_flag = CMD;
					}
				}
				else if ((bus.servo[i].cur_load >= minWheelTorque && bus.servo[i].cur_load <= maxWheelTorque) || activate == 1) { //if feels a load, or other gripper feels a load, activate grippers;
					if(bus.servo[i].cur_load < -.4)
					{
						gripped[i] = 1;
					}
					activate = 1;
					bus.servo[i].cmd_speed = 0.5;
					bus.servo[i].cmd_flag = CMD;
				}
				break;
		}
        if (gripped[0] == 1 && gripped[1] == 1) {
            perchstate.gripped = 1;
			//perchstate.release=1;
        }
        if (gripped[0] == 0 && gripped[1] == 0) {
            perchstate.gripped = 0;
			if(perchstate.release)
			{
				perchstate.release = 0;
				activate = 0;
			}
        }
		pthread_mutex_lock(&perch_mutex);
		memcpy( perch, &perchstate,sizeof(struct perch));
		pthread_mutex_unlock(&perch_mutex);
        pthread_mutex_unlock(&dynamixel_mutex);
        usleep(1000000/hz);
    }

    /* Deinitialize */
    Dynam_Deinit(&dynam);

}
