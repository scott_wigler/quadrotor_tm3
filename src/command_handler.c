// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata)
{
  static int armed = 1;
  // create a copy of the received message
  channels_t new_msg;
  new_msg.utime = msg->utime;
  new_msg.num_channels = msg->num_channels;
  new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
  for(int i = 0; i < msg->num_channels; i++){
    new_msg.channels[i] = msg->channels[i];
  }

  // Copy state to local state struct to minimize mutex lock time
  struct state localstate;
  struct perch perchstate;
  pthread_mutex_lock(&state_mutex);
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);


  if (msg->channels[7] > 1800)
  {
	  if(localstate.fence_on == 0)
	  {
		  printf("Fence turned on\n");

		  localstate.set_points[0] = localstate.pose[0];
		  localstate.set_points[1] = localstate.pose[1];
		  localstate.set_points[2] = localstate.pose[2];
		  localstate.set_points[3] = localstate.pose[3];
		  //waypoints[0][0] = localstate.pose[0];
		  //waypoints[0][1] = localstate.pose[1];
		  //waypoints[0][2] = localstate.pose[2];
		  //waypoints[0][3] = localstate.pose[3];
		  //waypoints[0][8] = 1;
//
		  //waypoints[0][0] = localstate.pose[0];
		  //waypoints[0][1] = localstate.pose[1]+1.5;
		  //waypoints[0][2] = localstate.pose[2];
		  //waypoints[0][3] = localstate.pose[3];
		  //waypoints[0][8] = 2;
	  }
	  localstate.fence_on = 1;
  }else
  {
	  if(localstate.fence_on == 1)
		  printf("Fence turned off\n");
	  localstate.fence_on = 0;
  }
  pthread_mutex_lock(&state_mutex);
  memcpy(state, &localstate, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);

  pthread_mutex_lock(&perch_mutex);
  memcpy(&perchstate, perch, sizeof(struct perch));
  pthread_mutex_unlock(&perch_mutex);
  if(perchstate.gripped)
  {
	  if(armed & !perchstate.release)
	  {
		  new_msg.channels[0] = 0;
		  new_msg.channels[1] = 2000;
		  new_msg.channels[2] = 0;
		  new_msg.channels[3] = 0;
		  sleep(2);
		  armed = 0;
	  }
	  if(msg->channels[0] > 1800)
	  {
		  perchstate.release = 1;
		  if(!armed)
		  {
			  new_msg.channels[0] = 0;
			  new_msg.channels[1] = 2000;
			  new_msg.channels[2] = 0;
			  new_msg.channels[3] = 0;
			  sleep(2);
			  armed = 1;
			  (cpoint)++;
		  }
	  }

  }
  pthread_mutex_lock(&perch_mutex);
  memcpy( perch, &perchstate,sizeof(struct perch));
  pthread_mutex_unlock(&perch_mutex);
  // Decide whether or not to edit the motor message prior to sending it
  // set_points[] array is specific to geofencing.  You need to add code
  // to compute them for our FlightLab application!!!
  float pose[8], set_points[8];
  if(localstate.fence_on == 1 && perchstate.gripped == 0){
	for(int i = 0; i < 8; i++){
	  pose[i] = (float) localstate.pose[i];
	  set_points[i] = localstate.set_points[i];
	}
	// hold position at edge of fence
	// This needs to change - mutex held way too long
	auto_control(pose, set_points, new_msg.channels);
	//printf("CH: FENCE ON\n");

  } else{  // Fence off
    // pass user commands through without modifying
    //printf("CH: FENCE OFF\n");
  }

  // send lcm message to motors
  new_msg.channels[7] = 1180;
  channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);

  // Save received (msg) and modified (new_msg) command data to file.
  // NOTE:  Customize as needed (set_points[] is for geofencing)
  fprintf(block_txt,"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f\n",
	  (long int) msg->utime,msg->channels[0],msg->channels[1],msg->channels[2],
	  msg->channels[3], msg->channels[7],
	  new_msg.channels[0],new_msg.channels[1],new_msg.channels[2],
	  new_msg.channels[3],new_msg.channels[7],
	  set_points[0],set_points[1],set_points[2],
	  set_points[3],set_points[4],set_points[5],set_points[6],
	  set_points[7]);
  fflush(block_txt);
}
