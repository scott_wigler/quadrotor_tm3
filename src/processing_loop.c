// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#include "../include/quadcopter_main.h"
void read_config(char *);
int processing_loop_initialize();
float waypoints[9][9] = {0};
/*
 * State estimation and guidance thread
*/
void *processing_loop(void *data){

  int hz = PROC_FREQ;
  processing_loop_initialize();

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;

  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);
  pthread_mutex_lock(&state_mutex);
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);

    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;

    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    if(mc_time_step > 1.0E-7 && mc_time_step < 1){
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
    }
	//localstate.fence_on =1;
    //localstate.time = utime_now();


  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controller
    if(localstate.fence_on == 0 ){
      // ADD YOUR CODE HERE
	  //printf("Fence off\n");
    }
     // end else if (localstate.fence_on == 1 && timed out)

    else if(localstate.fence_on == 1){
      // update desired state
      update_set_points(localstate.pose, localstate.set_points,&cpoint);
    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);

  } // end while processing_loop()

  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int *cpoint){
  //Add your code here to generate proper reference state for the controller

//printf("updating set\n");
  //printf("setp %f %f %f\n", pose[0], pose[1], pose[2]);
  if(norml2(pose, set_points, 3) < .15)
  {
	//Go to next point
	if(waypoints[*cpoint][8] > 0)
	{
		printf("%f\n",waypoints[*cpoint][8]);
		printf("Next waypoint %d\n", *cpoint);
		(*cpoint)++;
		//localstate.set_points[0] = waypoints[*cpoint][0];
		//localstate.set_points[1] = waypoints[*cpoint][1];
		//localstate.set_points[2] = waypoints[*cpoint][2];
		//localstate.set_points[3] = waypoints[*cpoint][3];

	}
  }


  return 0;
}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize()
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));

  // Read configuration file
  char blah[] = "config.txt";
  read_config(blah);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;

  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;
}

void read_config(char* config){
  // open configuration file
  FILE* conf = fopen(config,"r");
  // holder string
  char str[1000];
  char *line;
  size_t len;

  // read in the quadrotor initial position
  //fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
  if(conf == NULL)
	  return;
  printf("reading %s\n", config);
  fscanf(conf,"%s %f %f %f",str,&KP_thrust,&KI_thrust,&KD_thrust);
  printf("%s %f %f %f\n",str,KP_thrust,KI_thrust,KD_thrust);
  fscanf(conf,"%s %f %f %f",str,&KP_pitch,&KI_pitch,&KD_pitch);
  printf("%s %f %f %f\n",str,KP_pitch,KI_pitch,KD_pitch);
  fscanf(conf,"%s %f %f %f",str,&KP_roll,&KI_roll,&KD_roll);
  printf("%s %f %f %f\n",str,KP_roll,KI_roll,KD_roll);
  fscanf(conf,"%s %f %f %f",str,&KP_yaw,&KI_yaw,&KD_yaw);
  printf("%s %f %f %f\n",str,KP_yaw,KI_yaw,KD_yaw);

  float tp[8];
  int i = 0;

  while(fscanf(conf,"%s %f %f %f %f %f %f %f %f",str,&tp[0], &tp[1], &tp[2], &tp[3], &tp[4], &tp[5], &tp[6], &tp[7]) != -1 && i < 9)
  {
	  waypoints[i][0] = tp[0];
	  waypoints[i][1] = tp[1];
	  waypoints[i][2] = tp[2];
	  waypoints[i][3] = tp[3];
	  waypoints[i][4] = tp[4];
	  waypoints[i][5] = tp[5];
	  waypoints[i][6] = tp[6];
	  waypoints[i][7] = tp[7];
	  waypoints[i][8] = i+1;
  	  printf("%s %f %f %f %f %f %f %f %f\n",str,tp[1], tp[2], tp[3], tp[4], tp[5], tp[6], tp[7]);
	  i++;
  }

  fclose(conf);
}
