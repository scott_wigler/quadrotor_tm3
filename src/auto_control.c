//
// auto_control:  Function to generate autonomous control PWM outputs.
//
#define EXTERN extern
#include "../include/quadcopter_main.h"
#include "../include/pid.h"

// Define outer loop controller
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1500 // Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value.
#define thrust_PWM_down 1400 // Lower saturation PWM limit.

// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value.
#define roll_PWM_right 1380 //Right saturation PWM limit.

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value.
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit.

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value.
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right).

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control(float *pose, float *set_points, int16_t* channels_ptr)
{
  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!)
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot}

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw
	//static pid_val thrust_pid = {.kp = KP_thrust, .ki = KI_thrust, .kd = KD_thrust, .out_min = thrust_PWM_down, .out_max = thrust_PWM_up, .i_min = 1470, .i_max = 1530};
	//static pid_val roll_pid = {.kp = KP_roll, .ki = KI_roll, .kd = KD_roll, .out_min = roll_PWM_right, .out_max = roll_PWM_left, .i_min = 1470, .i_max = 1530};
	//static pid_val pitch_pid = {.kp = KP_pitch, .ki = KI_pitch, .kd = KD_pitch, .out_min = pitch_PWM_backward, .out_max = pitch_PWM_forward, .i_min = 1470, .i_max = 1530};
	//static pid_val yaw_pid = {.kp = KP_yaw, .ki = KI_yaw, .kd = KD_yaw, .out_min = yaw_PWM_cw, .out_max = yaw_PWM_ccw, .i_min = 1470, .i_max = 1530};

	static pid_val thrust_pid = {KP_thrust, KI_thrust, KD_thrust, thrust_PWM_down, thrust_PWM_up, 1470,  1530, 0,thrust_PWM_base,.02};
	static pid_val roll_pid = {KP_roll, KI_roll, KD_roll, roll_PWM_right, roll_PWM_left,  1470,  1530, 0,roll_PWM_base,.02};
	static pid_val pitch_pid = {KP_pitch, KI_pitch, KD_pitch, pitch_PWM_backward, pitch_PWM_forward, 1470, 1530, 0,pitch_PWM_base,.02};
	static pid_val yaw_pid = {KP_yaw, KI_yaw, KD_yaw, yaw_PWM_cw, yaw_PWM_ccw, 1470, 1530,0, yaw_PWM_base,.02};
	static int64_t timec = utime_now();
	float dt  = (utime_now() - timec)/1e6;
	timec = utime_now();

	yaw_pid.dt = pitch_pid.dt = roll_pid.dt = thrust_pid.dt = dt;

	thrust_pid.error = -(set_points[2] - pose[2]);
	roll_pid.error = -(set_points[1] - pose[1]);
	 pitch_pid.error =(set_points[0] - pose[0]);
	yaw_pid.error = -(set_points[3] - pose[3]);

	p(&thrust_pid);
	p(&roll_pid);
	p(&pitch_pid);
	p(&yaw_pid);

	//printf("\t\t%d\t%d\t%d\t%d\n", channels_ptr[0], channels_ptr[1], channels_ptr[2], channels_ptr[3]);
	//channels_ptr[0] = thrust_pid.output;
	channels_ptr[1] = roll_pid.output;
	channels_ptr[2] = pitch_pid.output;
	//channels_ptr[3] = yaw_pid.output;
	//printf("Pose %f %f %f %f",pose[2],pose[1],pose[0],pose[3]);

	//printf("\t\t%f\t%f\t%f\t%f\n", thrust_pid.output, roll_pid.output, pitch_pid.output, yaw_pid.output);
	printf("\t\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", pitch_pid.output, roll_pid.output, thrust_pid.output, yaw_pid.output, pose[0], pose[1], pose[2], pose[3]);

	thrust_pid.error_prev = thrust_pid.error;
	roll_pid.error_prev = thrust_pid.error;
	pitch_pid.error_prev = thrust_pid.error;
	yaw_pid.error_prev = thrust_pid.error;
  return;
}
