// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern

#include "../include/quadcopter_main.h"
/*
 * State estimation and guidance thread
*/
void *run_log(void *data){
  lcm_t* lcm = lcm_create(NULL);
  FILE* block = fopen("flight_test_5/block.txt","r");
  FILE* mcap = fopen("flight_test_5/mcap.txt","r");
  struct motion_capture_obs mc; // Local copy of mcap_obs object (to update)
  // holder string
  char str[2000];
  int64_t timec = utime_now();
  int64_t mcaptz=0, blocktz=0;
  double mcap_temp[7], block_temp[19];
  if(block == NULL || mcap == NULL)
  {
	  printf("bad log file reading\n");
	  return 0;
  }
  fscanf(mcap,"%lf,%lf,%lf,%lf,%lf,%lf,%lf",&mcap_temp[0],&mcap_temp[1],&mcap_temp[2],&mcap_temp[3],&mcap_temp[4],&mcap_temp[5],&mcap_temp[6]);
  fscanf(block,"%lf,%lf,%lf,%lf,%lf,%lf",&block_temp[0],&block_temp[1],&block_temp[2],&block_temp[3],&block_temp[4],&block_temp[5]);fscanf(block,"%s", str);
  mcaptz = mcap_temp[0]*1e6;
  blocktz = block_temp[0];
//

  while(1)
  {
	if(utime_now()-timec > (mcap_temp[0]*1e6 - mcaptz))
	{

		mc.pose[0] = mcap_temp[1];
		mc.pose[1] = mcap_temp[2];
		mc.pose[2] = mcap_temp[3];
		mc.pose[3] = mcap_temp[4];
		mc.pose[4] = mcap_temp[5];
		mc.pose[5] = mcap_temp[6];
		//pthread_mutex_lock(&mcap_mutex);
		//memcpy(mcap_obs+1, mcap_obs, sizeof(struct motion_capture_obs));
		//memcpy(mcap_obs, &mc, sizeof(struct motion_capture_obs));
		//pthread_mutex_unlock(&mcap_mutex);
  		fscanf(mcap,"%lf,%lf,%lf,%lf,%lf,%lf,%lf",&mcap_temp[0],&mcap_temp[1],&mcap_temp[2],&mcap_temp[3],&mcap_temp[4],&mcap_temp[5],&mcap_temp[6]);
	}


	if(utime_now()-timec > (block_temp[0] - blocktz))
	{
		channels_t new_msg;

		  new_msg.utime = block_temp[0];
		  new_msg.num_channels = 8;
		  new_msg.channels = (int16_t*) malloc(new_msg.num_channels*sizeof(int16_t));
		  new_msg.channels[0] = mcap_temp[1];
		  new_msg.channels[1] = mcap_temp[2];
		  new_msg.channels[2] = mcap_temp[3];
		  new_msg.channels[3] = mcap_temp[4];
		  //new_msg.channels[7] = mcap_temp[5];
		  new_msg.channels[7] = 1900;
		  channels_handler(NULL,NULL,&new_msg, lcm);
		  fscanf(block,"%lf,%lf,%lf,%lf,%lf,%lf",&block_temp[0],&block_temp[1],&block_temp[2],&block_temp[3],&block_temp[4],&block_temp[5]);fscanf(block,"%s", str);
	}
    usleep(100);
  }

  fclose(block);
  fclose(mcap);



  return 0;
}

